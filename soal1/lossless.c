#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include<wait.h>

#define MAX_TREE_HT 10000


struct hnd {
	char data;
	unsigned freq;
	struct hnd *left, *right;
};

struct h {
	unsigned size;
	unsigned capacity;
	struct hnd** array;
};

struct hnd* newnd(char data, unsigned freq)
{
	struct hnd* temp = (struct hnd*)malloc(
		sizeof(struct hnd));

	temp->left = temp->right = NULL;
	temp->data = data;
	temp->freq = freq;

	return temp;
}

struct h* createh(unsigned capacity){
	struct h* h = (struct h*)malloc(sizeof(struct h));
	h->size = 0;
	h->capacity = capacity;
	h->array = (struct hnd**)malloc(
	h->capacity * sizeof(struct hnd*));
	return h;
}

void swaphnd(struct hnd** a,struct hnd** b){
	struct hnd* t = *a;
	*a = *b;
	*b = t;
}
void hify(struct h* h, int idx){

	int smallest = idx;
	int left = 2 * idx + 1;
	int right = 2 * idx + 2;

	if (left < h->size
		&& h->array[left]->freq
			< h->array[smallest]->freq)
		smallest = left;

	if (right < h->size
		&& h->array[right]->freq
			< h->array[smallest]->freq)
		smallest = right;

	if (smallest != idx) {
		swaphnd(&h->array[smallest],
						&h->array[idx]);
		hify(h, smallest);
	}
}

int isSizeOne(struct h* h){

	return (h->size == 1);
}

struct hnd* extractMin(struct h* h){

	struct hnd* temp = h->array[0];
	h->array[0] = h->array[h->size - 1];

	--h->size;
	hify(h, 0);

	return temp;
}

void inserth(struct h* h, struct hnd* hnd){

	++h->size;
	int i = h->size - 1;

	while (i
		&& hnd->freq
				< h->array[(i - 1) / 2]->freq) {

		h->array[i] = h->array[(i - 1) / 2];
		i = (i - 1) / 2;
	}

	h->array[i] = hnd;
}

// A standard function to build min heap
void buildh(struct h* h)

{

	int n = h->size - 1;
	int i;

	for (i = (n - 1) / 2; i >= 0; --i)
		hify(h, i);
}

// A utility function to print an array of size n
int printArr(int arr[], int n)
{
	int i, c=0;
	for (i = 0; i < n; ++i){
		//printf("%d ", arr[i]);
        c++;
    }
    //printf("%d ", c);
	return c;
}

// Utility function to check if this nd is leaf
int isLeaf(struct hnd* root)

{

	return !(root->left) && !(root->right);
}

struct h* createAndBuildh(char data[], int freq[], int size){

	struct h* h = createh(size);

	for (int i = 0; i < size; ++i)
		h->array[i] = newnd(data[i], freq[i]);

	h->size = size;
	buildh(h);

	return h;
}

// The main function that builds Huffman tree
struct hnd* buildHuffmanTree(char data[], int freq[], int size){
	struct hnd *left, *right, *t;

	
	struct h* h= createAndBuildh(data, freq, size);


	while (!isSizeOne(h)) {

	
		left = extractMin(h);
		right = extractMin(h);

		t = newnd('$', left->freq + right->freq);

		t->left = left;
		t->right = right;

		inserth(h, t);
	}

	// Step 4: The remaining nd is the
	// root nd and the tree is complete.
	return extractMin(h);
}

// Prints huffman codes from the root of Huffman Tree.
// It uses arr[] to store codes
void printCodes(struct hnd* root, int arr[], int t, int bitz[]){
  
	if (root->left) {

		arr[t] = 0;
		printCodes(root->left, arr, t + 1, bitz);
	}
	if (root->right) {

		arr[t] = 1;
		printCodes(root->right, arr, t + 1, bitz);
	}
     if (isLeaf(root)) {
        //printf("%c: ", root->data);
        int c = printArr(arr, t);
        int b = root->data - 'A';
        bitz[b] = c;
		//printf("  %d\n",bitz[b] );
    }
}

void HuffmanCodes(char data[], int frq[], int size, int bitz[]){

	struct hnd* root = buildHuffmanTree(data, frq, size);
	int arr[MAX_TREE_HT], t = 0;    
//   for (int i = 0; i<26;i++){
//         printf("%d\n", frq[i]);
//     }
	printCodes(root, arr, t, bitz);
}


int readfile(char* argv, int frq[], char let[], int bit){

    FILE *fp;
    int c, i=0;

    fp = fopen(argv, "r");
    if (fp == NULL) {
        printf("Error opening the file\n");
        return 0;
    }

    while ((c = fgetc(fp)) != EOF) {
        if (c >= 'a' && c <= 'z') {
            bit += 8;
            frq[c - 'a']++;
        } else if (c >= 'A' && c <= 'Z') {
            bit += 8;
            frq[c - 'A']++;
        }
        
        i++;
    }

    fclose(fp);

    for (int i = 0; i < 26; i++) {
        let[i] = 'A' + i;
        //printf("%c: %d\n", let[i], frq[i]);
    }
    return bit;
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Usage: %s <filename>\n", argv[0]);
        return 1;
    }

  int pfd[2];
  int cfd[2];
    if (pipe(pfd) == -1) {
        printf("Pipe failed\n");
        exit(1);
    }
	if (pipe(cfd) == -1) {
        printf("Pipe failed\n");
        exit(1);
    }
    // create child process
    pid_t pd = fork();
    if(pd==0){
       close(pfd[1]);
 int frq[26] = {0}, sz = sizeof(frq)/sizeof(frq[0]), bitz[]= {0} ;
  char let[26] = {0};
        if (read(pfd[0], let, sizeof(let)) < 0 ||
            read(pfd[0], frq, sizeof(frq)) < 0)
        {
            printf("Failed to read arrays from parent process\n");
            exit(1);
        }
		wait(NULL);
    
	close(pfd[0]);
	close(cfd[0]);
	HuffmanCodes(let,frq,26, bitz);
		 if (write(cfd[1], bitz, sizeof(bitz)) < 0 )
        {
            printf("Failed to send data to child process\n");
            exit(1);
        }
		wait(NULL);
		int a = 0;
			  for (int i = 0; i<26;i++){
        a +=  bitz[i] * frq[i];
    }

        write(cfd[1], &a, sizeof(a));
		wait(NULL);
        close(cfd[1]);
		printf("%d BIT SETELAH HUFFMAN\n", a);
    } else {
        close(pfd[0]);
    int frq[26] = {0}, sz = sizeof(frq)/sizeof(frq[0]), bit = 0;
  char let[26] = {0};
    bit = readfile(argv[1], frq, let, bit);
    if (write(pfd[1], let, sizeof(let)) < 0 ||
            write(pfd[1], frq, sizeof(frq)) < 0)
        {
            printf("Failed to send data to child process\n");
            exit(1);
        }
		
        close(pfd[1]);
		close(cfd[1]);
		printf("%d BIT SEBELUM HUFMAN\n", bit);
		wait(NULL);
		int bitz[26] = {0};
if (read(cfd[0], bitz, sizeof(bitz)) < 0 )
{
    printf("Failed to read data from child process\n");
    exit(1);
}
wait(NULL);

        int a;
        read(cfd[0], &a, sizeof(a));
        close(cfd[0]);
	//   for (int i = 0; i<26;i++){
    //     printf("%d\n", bitz[i]);
    // }
        wait(NULL);
        

    }
    return 0;
}