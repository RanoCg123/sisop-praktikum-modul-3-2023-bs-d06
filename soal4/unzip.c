#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pwd.h>
#include <wait.h>
#include <sys/types.h>

void remove_file(char *filename) {
  pid_t rmpid;
  rmpid = fork();
  char *argv[] = {"rm", "-q", filename, NULL};
  if(rmpid == 0) {
    execv("/usr/bin/rm", argv);
  }
  while(wait(NULL) != rmpid);
}

void unzip(char *filename) {
  pid_t uzpid;
  uzpid = fork();
  char *argv[] = {"unzip", "-q", filename, NULL};
  if(uzpid == 0) {
    execv("/usr/bin/unzip", argv);
  }
  while(wait(NULL) != uzpid);
  remove(filename);
}

void download() {
  pid_t dlpid;
  dlpid = fork();
  char *argv[] = {"wget", "https://drive.google.com/uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp", "-q", NULL};
  if(dlpid == 0) {
    execv("/usr/bin/wget", argv);
  }
  while(wait(NULL) != dlpid);
}

int main(void) {
  pid_t main_fork;
  main_fork = fork();
  if(main_fork == 0) {
    download();
    exit(0);
  }
  while(wait(NULL) != main_fork);

  main_fork = fork();
  if(main_fork == 0) {
    unzip("uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp");
    exit(0);
  }
  while(wait(NULL) != main_fork);

  printf("done\n");
  return 0;
}
