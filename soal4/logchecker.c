#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

int main() {
  FILE *log_reader = fopen("log.txt", "r");
  int access = 0, folder_max = 0, limit = 0, folder_count[100];
  char folder_list[100][20];
  while(limit != 100) {
    char word[255];
    fscanf(log_reader, "%s", word);
    //printf("%s\n", word);
    if(!strcmp(word, "ACCESSED")) {
      access++;
    }
    else if(!strcmp(word, "MADE")) {
      fscanf(log_reader, "%s", folder_list[folder_max]);
      folder_count[folder_max] = 0;
      folder_max++;
    }
    else if(!strcmp(word, "MOVED")) {
      char char_moved[5];
      fscanf(log_reader, "%s", word);
      fscanf(log_reader, "%s", char_moved);
      if(!strcmp(char_moved, ">")) {
        char location[255];
        fscanf(log_reader, "%s", location);
        for(int i = 0; i < folder_max; i++) {
          if(!strcmp(folder_list[i], location)) {
            folder_count[i]++;
            break;
          }
        }
      }
    }
    limit++;
  }
  printf("total accessed: %d\n", access);
  for(int i = 0; i < folder_max; i++) {
    printf("%s     : %d\n", folder_list[i], folder_count[i]);
  }
  fclose(log_reader);
  return 0;
}
