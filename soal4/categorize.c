#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <pwd.h>
#include <sys/stat.h>
#include <unistd.h>
#include <wait.h>
#include <pthread.h>
#include <stdbool.h>
#include <ctype.h>

typedef struct {
  char filename[512];
  char data_type[20];
} moving;

int read_extensions() {
  FILE *dt = fopen("extensions.txt", "r");
  char data_type[20][20];
  int type_count = 0;
  while (fgets(data_type[type_count], 20, dt)) {
    type_count++;
  }
  fclose(dt);
  return type_count;
}

bool hasExtension(char filename[], char data_type[]) {
  char *dot = strrchr(filename, '.');
  if (dot && !strcmp(dot + 1, data_type)) {
    return 1;
  }
  return 0;
}

void log_access(char *log_type, char *extension, char *target, char *destination) {
  FILE *dt7 = fopen("log.txt", "w+");
  time_t time_;
  struct tm *time_now;
  char timestamp[24];

  time_ = time(NULL);
  time_now = localtime(&time_);
  strftime(timestamp, sizeof(timestamp), "%d-%m-%Y %H:%M:%S", time_now);

  if(!strcmp(log_type, "ACCESSED")) {
    fprintf(dt7, "%s %s %s\n", timestamp, "ACCESSED", target);
  } else if(!strcmp(log_type, "MOVED")) {
    fprintf(dt7, "%s %s %s file : %s > %s\n", timestamp, "MOVED", extension, target, destination);
  } else if(!strcmp(log_type, "MADE")) {
    fprintf(dt7, "%s %s %s\n", timestamp, "MADE", destination);
  }
  fclose(dt7);
}

void *move_file(void *arg) {
    moving *file = (moving *) arg;

    char dir_name[255];
    snprintf(dir_name, sizeof(dir_name), "./categorized/%s", file->data_type);

    char *filename_s = strrchr(file->filename, '/');
    if (access(dir_name, F_OK) == -1) {
        mkdir(dir_name, 0777);
        log_access("MADE", NULL, NULL, dir_name);
    }

    char command[1400];
    snprintf(command, sizeof(command), "mv %s %s%s", file->filename, dir_name, filename_s);
    system(command);

    log_access("ACCESSED", NULL, file->filename, NULL);
    log_access("MOVED", file->data_type, file->filename, dir_name);

    free(file);
    pthread_exit(NULL);
}

void countExtensions() {
  char data_type[20][20];
  FILE *dt2 = fopen("extensions.txt", "r");

  int type_count = 0;
  while (type_count != 8) {
    fscanf(dt2, "%s", data_type[type_count]);
    printf("%s\n", data_type[type_count]);
    type_count++;
  }
  fclose(dt2);

  int extension_total = type_count;
  int counter[extension_total + 1];
  for (int i = 0; i <= extension_total; i++) {
    counter[i] = 0;
  }
  FILE *dt4 = fopen("all_file.txt", "r");
  int total_file = 0;
  while(total_file != 200) {
    char data_type2[10], file_location2[255];
    fscanf(dt4, "%s", file_location2);
    FILE *dt3 = fopen("extensions.txt", "r");
    int foundExtension = 0;
    for (int i = 0; i < extension_total; i++) {
      fscanf(dt3, "%s", data_type2);
      //printf("%s %s\n", file_location2, data_type2);
      if(hasExtension(file_location2, data_type2)) {
        foundExtension = 1;
        counter[i]++;
        break;
      }
    }
    if (!foundExtension) {
      counter[extension_total]++;
    }
    fclose(dt3);
    total_file++;
  }
  fclose(dt4);

  printf("File Extension\n");
  for (int i = 0; i < extension_total; i++) {
    printf("%s    : %d\n", data_type[i], counter[i]);
  }
  printf("Other : %d\n", counter[extension_total]);
}


int main() {
  system("mkdir categorized; date +%d-%m-%Y %H:%M:%S MADE categorized\n > log.txt; find ./files -type f > all_file.txt");
  countExtensions();

  pthread_t thread_id[1024];
  int thread_count = 0, total_file2 = 0;
  int type_total2 = read_extensions();
  FILE *dt5 = fopen("all_file.txt", "r");
  while(total_file2 != 200) {
    char file_location3[255], data_type3[255];
    fscanf(dt5, "%s", file_location3);
    FILE *dt6 = fopen("extensions.txt", "r");
    for(int i = 0; i < type_total2; i++) {
      fscanf(dt6, "%s", data_type3);
      if(hasExtension(file_location3, data_type3)) {
        moving *file = malloc(sizeof(moving));
        strcpy(file->filename, file_location3);
        strcpy(file->data_type, data_type3);

        pthread_create(&thread_id[thread_count], NULL, move_file, file);
        thread_count++;
        break;
      }
    }
  }
  for(int i = 0; i < thread_count; i++) {
    pthread_join(thread_id[i], NULL);
  }
  fclose(dt5);

  return 0;
}

