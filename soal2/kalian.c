#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>
int main() {
    int arm1[4][2];
    int arm2[2][5];
    int res[4][5];
    int i, j, k;
  int smid;
    key_t key = 4869;
    int *sm;
    // Initialize random seed
    srand(time(NULL));

    // Fill arm1 with random numbers between 1-5
    for(i=0;i<4;i++) {
        for(j=0;j<2;j++) {
            arm1[i][j] = rand() % 5 + 1;
        }
    }

    // Fill arm2 with random numbers between 1-4
    for(i=0;i<2;i++) {
        for(j=0;j<5;j++) {
            arm2[i][j] = rand() % 4 + 1;
        }
    }

    // Display array matrix 1
    printf("\n");
    for(i=0;i<4;i++) {
        for(j=0;j<2;j++) {
            printf("%d ", arm1[i][j]);
        }
        printf("\n");
    }

    // Display array matrix 2
    printf("\n");
    for(i=0;i<2;i++) {
        for(j=0;j<5;j++) {
            printf("%d ", arm2[i][j]);
        }
        printf("\n");
    }

    // Dikasi dan dimasuukan ke array result
    for(i=0;i<4;i++) {
        for(j=0;j<5;j++) {
            res[i][j] = 0;
            for(k=0;k<2;k++) {
                res[i][j] += arm1[i][k] * arm2[k][j];
            }
        }
    }
    // printf("\n");
    // for(i=0;i<4;i++) {
    //     for(j=0;j<5;j++) {
    //         printf("%d ", res[i][j]);
    //     }
    //     printf("\n");
    // }
    smid = shmget(key, sizeof(int) * 4 * 5, IPC_CREAT | 0666);
    if (smid < 0) {
        perror("shmget");
        exit(1);
    }
    sm = shmat(smid, NULL, 0);
    if (sm == (int *) -1) {
        perror("shmat");
        exit(1);
    }
    for (i = 0; i < 4; i++) {
    for (j = 0; j < 5; j++) {
        *(sm + i * 5 + j) = res[i][j];
    }
}

shmdt(sm);
    return 0;
}