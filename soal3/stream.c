#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <openssl/evp.h>
#include <cjson/cJSON.h>

void hex_decode(const char *input, char *output) {
    int len = strlen(input);
    int i, j;

    for (i = 0, j = 0; i < len; i += 2, j++) {
        sscanf(input + i, "%2hhx", &output[j]);
    }

    output[j] = '\0';
}

int main(){
    printf("Enter Command : ");
    char comm[10];
    scanf("%s", comm);
    if(strcmp(comm,"decrypt")== 0)
    {
        FILE *inputFile = fopen("song-playlist.json", "r");
        if (!inputFile) {
            printf("Failed to open input file\n");
            return 1;
        }

        FILE *outputFile = fopen("sort.txt", "w");
        if (!outputFile) {
            printf("Failed to open output file\n");
            return 1;
        }

        char buffer[100000];
        fread(buffer, 1, 100000, inputFile);
        fclose(inputFile);

        cJSON *json = cJSON_Parse(buffer);
        if (!json) {
            printf("Failed to parse JSON\n");
            return 1;
        }

        int jsonSize = cJSON_GetArraySize(json);
        for (int i = 0; i < jsonSize; i++) {
            cJSON *item = cJSON_GetArrayItem(json, i);
            cJSON *songItem = cJSON_GetObjectItem(item, "song");
            cJSON *methodItem = cJSON_GetObjectItem(item, "method");
    
            char *method = methodItem->valuestring;
            char *song = songItem->valuestring;

            if (strcmp(method, "rot13") == 0) {
                for (int i = 0; i < strlen(song); i++) {
                    if (song[i] >= 'a' && song[i] <= 'z') {
                        song[i] = (song[i] - 'a' + 13) % 26 + 'a';
                    } else if (song[i] >= 'A' && song[i] <= 'Z') {
                        song[i] = (song[i] - 'A' + 13) % 26 + 'A';
                    }
                }
            } else if (strcmp(method, "hex") == 0) {
                int songLength = strlen(song);
                char *decodedSong = malloc(300);
                hex_decode(song, decodedSong);
                strcpy(song, decodedSong);
                free(decodedSong);
            } else if (strcmp(method, "base64") == 0) {
                int songLength = strlen(song);
                int outLength = 0;
                char *decodedSong = malloc(300);
                EVP_DecodeBlock((unsigned char*)decodedSong, (unsigned char*)song, songLength);
                decodedSong[songLength] = '\0';
                strcpy(song, decodedSong);
                free(decodedSong);
            }
            fprintf(outputFile, "%s\n", song);
        }
        fclose(outputFile);
        cJSON_Delete(json);
        system("sort -f sort.txt > playlist.txt");
        system("rm sort.txt");
    }
    else if(strcmp(comm,"list")==0){
        system("cat playlist.txt");
    }else if(strcmp(comm,"play")==0){
        char sortir[200];
        scanf(" %[^\n]", sortir);
        char perintah[300];
        snprintf(perintah, 300, "grep -i '%s' playlist.txt > search.txt", sortir);
        system(perintah);

        FILE *count;
            char filename[] = "search.txt";
            char ch;
            int lines = 0;

            count = fopen(filename, "r");

            while ((ch = fgetc(count)) != EOF) {
                if (ch == '\n') {
                    lines++;
                }
            }

            fclose(count);
            if(lines > 1){
                printf("%d song founded within %s name: \n", lines, sortir);
                FILE *fp;
                char filename[] = "search.txt";
                char buffer[1000];
                int num = 0;

                fp = fopen(filename, "r");

                while(fgets(buffer, 1000, fp)) {
                    num++;
                    printf("%d. %s", num, buffer);
                }
                fclose(fp);

                printf("Choose song : ");
                char pilihan[100];
                scanf("%s",pilihan);

                char pilih[300];
                snprintf(pilih, 300, "grep -i '%s' search.txt > pilih.txt", pilihan);
                system(pilih);

                char filename2[] = "pilih.txt";
                
                fp = fopen(filename2, "r");
                
                while(fgets(buffer, 1000, fp)) {
                    printf("Now playing : %s", buffer);
                    break;
                }
                fclose(fp);
                }else{
                char outsearch[100];
                sprintf(outsearch, "cat search.txt | sed 's/^/play /'");
                system(outsearch);
            }
        system("rm search.txt");
        system("rm pilih.txt");
    }else if(strcmp(comm,"add")==0){
        char newsong[100];
        scanf(" %[^\n]", newsong);
        FILE *addnewsong = fopen("playlist.txt", "a");

        char perintah[150];
        snprintf(perintah, 200, "grep -i '%s' playlist.txt > search.txt", newsong);
        system(perintah);

        FILE *count;
        char filename[] = "search.txt";
        char ch;
        int baris = 0;

        count = fopen(filename, "r");

        while ((ch = fgetc(count)) != EOF) {
            if (ch == '\n') {
                baris++;
            }
        }

        fclose(count);

        if(baris >= 1){
            printf("SONG ALREADY ON PLAYLIST\n");
        }else if(baris < 1){
            fprintf(addnewsong, "%s\n", newsong);
            printf("%s successfully added\n", newsong);
        }
        system("rm search.txt");
        fclose(addnewsong);
        system("sort -f playlist.txt > newsong.txt");
        system("cp newsong.txt playlist.txt");
        system("rm newsong.txt");
    }
    return 0;
}