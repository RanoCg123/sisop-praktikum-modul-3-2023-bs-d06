# sisop-praktikum-modul-3-2023-BS-D06


## SOAL 1
Di soal 1 kita diminta untuk membuat kode yang akan mencompresi sebuah file dengan huffman code dan membandingkan bit dari file awal dan file hasil.

### lossless.c
Dikarenakan soal meminta menggunkan pipe untuk huffman code. kita perlu membuat struct huffman code yang disini saya cari referensi di geeksforgeeks. Setelah itu untuk pipe kita deklarasi pipe pipenya dan kita juga perlu deklarasi pid untuk fork kedua file. 

Dalam kode ini pertama kita perlu melakukan reading terhadap file di parent. Disini kita akan menggunakan fungsi readfile yang akan membuka file yang diinginkan dan menghitung bits dari file tersebut. disini kita menggunakan getchar dan membandingkan dengan nilai ascii. jika benar maka dalam array frequency akan ditambah sesuai dengan huruf yang diinginkan. Dalam kasus ini saya menggunakan kode 0 = a dan Z = 25. jadi ascii-A. Untuk mengitung bitnya sesuai soal dengan menambah 8 per karakter.

Lalu kita write array frekuensi dan array huruf ke child dan child. Child akan melakukan HuffmanCode untuk melakukan program huffman code dan menngkompresi file tersebut. 
Agar huffman code bisa berjalan kita perlu mendeklarasi array untuk frekuensi dan huruf. dan kita lalu membaca pipe dan memasukkan ke dalam array baru yang telah diklerasi.

Dalam menjalankan huffman code ketika dalam menjelajahi tree dan mencapai root kita melakukan loop untuk mengecek beberapa bit yang digunakan untuk huruf huruf tertentu dan disimpan dalam array. Sehingga ketika tree selesai kita menghitung banyak bits dalam file terkompresi dengan mengalikan bits dengan frekuensi. Hasilnya dikirim ke parent dan akan dibanding bit awal dan bit setelah huffman.

Hasilnya bisa dilihat hampir setengah dari ukuran awal.
```
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include<wait.h>

#define MAX_TREE_HT 10000


struct hnd {
	char data;
	unsigned freq;
	struct hnd *left, *right;
};

struct h {
	unsigned size;
	unsigned capacity;
	struct hnd** array;
};

struct hnd* newnd(char data, unsigned freq)
{
	struct hnd* temp = (struct hnd*)malloc(
		sizeof(struct hnd));

	temp->left = temp->right = NULL;
	temp->data = data;
	temp->freq = freq;

	return temp;
}

struct h* createh(unsigned capacity){
	struct h* h = (struct h*)malloc(sizeof(struct h));
	h->size = 0;
	h->capacity = capacity;
	h->array = (struct hnd**)malloc(
	h->capacity * sizeof(struct hnd*));
	return h;
}

void swaphnd(struct hnd** a,struct hnd** b){
	struct hnd* t = *a;
	*a = *b;
	*b = t;
}
void hify(struct h* h, int idx){

	int smallest = idx;
	int left = 2 * idx + 1;
	int right = 2 * idx + 2;

	if (left < h->size
		&& h->array[left]->freq
			< h->array[smallest]->freq)
		smallest = left;

	if (right < h->size
		&& h->array[right]->freq
			< h->array[smallest]->freq)
		smallest = right;

	if (smallest != idx) {
		swaphnd(&h->array[smallest],
						&h->array[idx]);
		hify(h, smallest);
	}
}

int isSizeOne(struct h* h){

	return (h->size == 1);
}

struct hnd* extractMin(struct h* h){

	struct hnd* temp = h->array[0];
	h->array[0] = h->array[h->size - 1];

	--h->size;
	hify(h, 0);

	return temp;
}

void inserth(struct h* h, struct hnd* hnd){

	++h->size;
	int i = h->size - 1;

	while (i
		&& hnd->freq
				< h->array[(i - 1) / 2]->freq) {

		h->array[i] = h->array[(i - 1) / 2];
		i = (i - 1) / 2;
	}

	h->array[i] = hnd;
}

// A standard function to build min heap
void buildh(struct h* h)

{

	int n = h->size - 1;
	int i;

	for (i = (n - 1) / 2; i >= 0; --i)
		hify(h, i);
}

// A utility function to print an array of size n
int printArr(int arr[], int n)
{
	int i, c=0;
	for (i = 0; i < n; ++i){
		//printf("%d ", arr[i]);
        c++;
    }
    //printf("%d ", c);
	return c;
}

// Utility function to check if this nd is leaf
int isLeaf(struct hnd* root)

{

	return !(root->left) && !(root->right);
}

struct h* createAndBuildh(char data[], int freq[], int size){

	struct h* h = createh(size);

	for (int i = 0; i < size; ++i)
		h->array[i] = newnd(data[i], freq[i]);

	h->size = size;
	buildh(h);

	return h;
}

// The main function that builds Huffman tree
struct hnd* buildHuffmanTree(char data[], int freq[], int size){
	struct hnd *left, *right, *t;

	
	struct h* h= createAndBuildh(data, freq, size);


	while (!isSizeOne(h)) {

	
		left = extractMin(h);
		right = extractMin(h);

		t = newnd('$', left->freq + right->freq);

		t->left = left;
		t->right = right;

		inserth(h, t);
	}

	// Step 4: The remaining nd is the
	// root nd and the tree is complete.
	return extractMin(h);
}

// Prints huffman codes from the root of Huffman Tree.
// It uses arr[] to store codes
void printCodes(struct hnd* root, int arr[], int t, int bitz[]){
  
	if (root->left) {

		arr[t] = 0;
		printCodes(root->left, arr, t + 1, bitz);
	}
	if (root->right) {

		arr[t] = 1;
		printCodes(root->right, arr, t + 1, bitz);
	}
     if (isLeaf(root)) {
        //printf("%c: ", root->data);
        int c = printArr(arr, t);
        int b = root->data - 'A';
        bitz[b] = c;
		//printf("  %d\n",bitz[b] );
    }
}

void HuffmanCodes(char data[], int frq[], int size, int bitz[]){

	struct hnd* root = buildHuffmanTree(data, frq, size);
	int arr[MAX_TREE_HT], t = 0;    
//   for (int i = 0; i<26;i++){
//         printf("%d\n", frq[i]);
//     }
	printCodes(root, arr, t, bitz);
}


int readfile(char* argv, int frq[], char let[], int bit){

    FILE *fp;
    int c, i=0;

    fp = fopen(argv, "r");
    if (fp == NULL) {
        printf("Error opening the file\n");
        return 0;
    }

    while ((c = fgetc(fp)) != EOF) {
        if (c >= 'a' && c <= 'z') {
            bit += 8;
            frq[c - 'a']++;
        } else if (c >= 'A' && c <= 'Z') {
            bit += 8;
            frq[c - 'A']++;
        }
        
        i++;
    }

    fclose(fp);

    for (int i = 0; i < 26; i++) {
        let[i] = 'A' + i;
        //printf("%c: %d\n", let[i], frq[i]);
    }
    return bit;
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Usage: %s <filename>\n", argv[0]);
        return 1;
    }

  int pfd[2];
  int cfd[2];
    if (pipe(pfd) == -1) {
        printf("Pipe failed\n");
        exit(1);
    }
	if (pipe(cfd) == -1) {
        printf("Pipe failed\n");
        exit(1);
    }
    // create child process
    pid_t pd = fork();
    if(pd==0){
       close(pfd[1]);
 int frq[26] = {0}, sz = sizeof(frq)/sizeof(frq[0]), bitz[]= {0} ;
  char let[26] = {0};
        if (read(pfd[0], let, sizeof(let)) < 0 ||
            read(pfd[0], frq, sizeof(frq)) < 0)
        {
            printf("Failed to read arrays from parent process\n");
            exit(1);
        }
		wait(NULL);
    
	close(pfd[0]);
	close(cfd[0]);
	HuffmanCodes(let,frq,26, bitz);
		 if (write(cfd[1], bitz, sizeof(bitz)) < 0 )
        {
            printf("Failed to send data to child process\n");
            exit(1);
        }
		wait(NULL);
		int a = 0;
			  for (int i = 0; i<26;i++){
        a +=  bitz[i] * frq[i];
    }

        write(cfd[1], &a, sizeof(a));
		wait(NULL);
        close(cfd[1]);
		printf("%d BIT SETELAH HUFFMAN\n", a);
    } else {
        close(pfd[0]);
    int frq[26] = {0}, sz = sizeof(frq)/sizeof(frq[0]), bit = 0;
  char let[26] = {0};
    bit = readfile(argv[1], frq, let, bit);
    if (write(pfd[1], let, sizeof(let)) < 0 ||
            write(pfd[1], frq, sizeof(frq)) < 0)
        {
            printf("Failed to send data to child process\n");
            exit(1);
        }
		
        close(pfd[1]);
		close(cfd[1]);
		printf("%d BIT SEBELUM HUFMAN\n", bit);
		wait(NULL);
		int bitz[26] = {0};
if (read(cfd[0], bitz, sizeof(bitz)) < 0 )
{
    printf("Failed to read data from child process\n");
    exit(1);
}
wait(NULL);

        int a;
        read(cfd[0], &a, sizeof(a));
        close(cfd[0]);
	//   for (int i = 0; i<26;i++){
    //     printf("%d\n", bitz[i]);
    // }
        wait(NULL);
        

    }
    return 0;
}
```
## SOAL 2
Pada Soal 2 kita diminta untuk membuat 3 file c. KIta diperlukan untuk menggunkaan shared memory dalam mengerjakan ini

### KALIAN.C
Di file ini kita harus membuat 2 array yang masing masing berukuran 2x5 dan 4x2 
Di array pertama berisi 1 - 5 dan array 2 berisi 1-4
Lalu kita perlu membuat shared memory dengan mendeklarasi key dan portnya.
setelah itu kita lakukan perkalian matriks dan masukkan hasil itu kedalam shared memory.
```
int main() {
    int arm1[4][2];
    int arm2[2][5];
    int res[4][5];
    int i, j, k;
  int smid;
    key_t key = 4869;
    int *sm;
    // Initialize random seed
    srand(time(NULL));

    // Fill arm1 with random numbers between 1-5
    for(i=0;i<4;i++) {
        for(j=0;j<2;j++) {
            arm1[i][j] = rand() % 5 + 1;
        }
    }

    // Fill arm2 with random numbers between 1-4
    for(i=0;i<2;i++) {
        for(j=0;j<5;j++) {
            arm2[i][j] = rand() % 4 + 1;
        }
    }

    // Display array matrix 1
    printf("\n");
    for(i=0;i<4;i++) {
        for(j=0;j<2;j++) {
            printf("%d ", arm1[i][j]);
        }
        printf("\n");
    }

    // Display array matrix 2
    printf("\n");
    for(i=0;i<2;i++) {
        for(j=0;j<5;j++) {
            printf("%d ", arm2[i][j]);
        }
        printf("\n");
    }

    // Dikasi dan dimasuukan ke array result
    for(i=0;i<4;i++) {
        for(j=0;j<5;j++) {
            res[i][j] = 0;
            for(k=0;k<2;k++) {
                res[i][j] += arm1[i][k] * arm2[k][j];
            }
        }
    }
    // printf("\n");
    // for(i=0;i<4;i++) {
    //     for(j=0;j<5;j++) {
    //         printf("%d ", res[i][j]);
    //     }
    //     printf("\n");
    // }
    smid = shmget(key, sizeof(int) * 4 * 5, IPC_CREAT | 0666);
    if (smid < 0) {
        perror("shmget");
        exit(1);
    }
    sm = shmat(smid, NULL, 0);
    if (sm == (int *) -1) {
        perror("shmat");
        exit(1);
    }
    for (i = 0; i < 4; i++) {
    for (j = 0; j < 5; j++) {
        *(sm + i * 5 + j) = res[i][j];
    }
}

shmdt(sm);
    return 0;
}
```
### CINTA.C
Dalam file ini kita pertama mendapatkan array dari shared memory sehingga kita perlu menggunakan key  dan port yang sama dan dimasukkan ke dalam local array.
Setelah itu kita display isi array dengan loop print.
Dalam kode ini kita perlu mengguankan thread sehingga perlu menginclude pthread.
Kita membuat 20 thread untuk masing masing bagian dari array. lalu tiap thread memiliki nilai args intrinsic agar dalam thread kita bisa megetahui isi array.
Dalam thread kita suruh untuk melakukan faktorisasi dalam sebuah fungsi pointer. Dalam pointer tersebut kita mengecek data intrinsic untuk mendapatkan nilai yang diperlukan array dan lakukan loop faktorisasi. jika sudah masukkan ke dalam array hasil.
Agar berurut kita perlu melakukan loop thread join untuk setiap thread untuk menunggu thread selesai faktorisasi.
Lalu kita display hasil faktorisasi.
```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <time.h>
int armat[4][5];
unsigned long long hasilf[20];
int c=0;
void *dofac(void *arg) {
    int *args = (int *) arg;
    int i = args[0];
    int j = args[1];
    int n = armat[i][j];
    unsigned long long res = 1;
    unsigned long long k;
    for (k = 2; k <= n; k++) {
        res *= k;
    }
    hasilf[c] = res;
    c++;
    pthread_exit(NULL);
}

int main() {
    int smid;
    key_t key = 4869;
    int *sm;
    int i, j;
    clock_t start, end;
    
 srand(time(NULL));
 start = clock();
    smid = shmget(key, sizeof(int) * 4 * 5, 0666);
    if (smid < 0) {
        perror("shmget");
        exit(1);
    }

    sm = shmat(smid, NULL, 0);
    if (sm == (int *) -1) {
        perror("shmat");
        exit(1);
    }
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            armat[i][j] = *(sm + i * 5 + j);
        }
    }
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            printf("%d ", armat[i][j]);
        }
        printf("\n");
    }
    printf("\n");

   
    shmdt(sm);
// Create threads to calculate dofac of each number in the armat
    pthread_t threads[20];
    int thread_args[20][2];
    int count = 0;
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            thread_args[count][0] = i;
            thread_args[count][1] = j;
            pthread_create(&threads[count], NULL, dofac, (void *) &thread_args[count]);
            count++;
        }
    }

    // Wait for all threads to finish
    for (i = 0; i < 20; i++) {
        pthread_join(threads[i], NULL);
    }
    printf("\narray: [");
for (i = 0; i < 4; i++) {
    printf("[");
        for (j = 0; j < 5; j++) {
            if (j== 4){
            printf("%d", armat[i][j]);
            }else printf("%d, ", armat[i][j]);
        }
        if(i==3){
            printf("]");
        }else printf("], ");
    }

    printf("] \n\nmaka:\n \n");
    for (i = 0; i < 20; i++) {
            printf("%llu ", hasilf[i]);
    }
    end = clock();
    printf("\ntime: %f sec\n", ((double) (end - start)) / CLOCKS_PER_SEC);
    return 0;
}

```
### SISOP.C
Dalam sisop.c kita melalukan hal yang sama untuk mendapatkan array seperti cinta.c dengan mengambil key dan port yang sama.
Display lagi isi array.
Lalu kita lakukan loop rekurisif untuk setiap isi dari array untuk mendapatkan nilai faktorisasi. dan kita display hasil faktorisasinya.
```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>
int armat[4][5];
unsigned long long factorial(unsigned long long n) {
    if (n == 0) {
        return 1;
    } else {
        return n * factorial(n-1);
    }
}
int main() {
    int smid;
    key_t key = 4869;
    int *sm;
    int i, j;
   clock_t start, end;
    
 srand(time(NULL));
 start = clock();
    smid = shmget(key, sizeof(int) * 4 * 5, 0666);
    if (smid < 0) {
        perror("shmget");
        exit(1);
    }

    sm = shmat(smid, NULL, 0);
    if (sm == (int *) -1) {
        perror("shmat");
        exit(1);
    }
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            armat[i][j] = *(sm + i * 5 + j);
        }
    }
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            printf("%d ", armat[i][j]);
        }
        printf("\n");
    }
    printf("\n");

    shmdt(sm);
    printf("\narray: [");
for (i = 0; i < 4; i++) {
    printf("[");
        for (j = 0; j < 5; j++) {
            if (j== 4){
            printf("%d", armat[i][j]);
            }else printf("%d, ", armat[i][j]);
        }
        if(i==3){
            printf("]");
        }else printf("], ");
    }
    printf("] \n\nmaka:\n \n");

    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            printf("%llu ", factorial(armat[i][j]));
        }
        printf(" ");
    }
 end = clock();
    printf("\ntime: %f sec\n", ((double) (end - start)) / CLOCKS_PER_SEC);
    return 0;
}

```
### KESIMPULAN 
Dengan menggunkan clock kita bisa mendapatkan waktu dari setiap kode. Dan kita mendapatkan hasil dimana cinta.c ternyata lebih cepat daripada sisop.c
Ini menunjukkan bahwa pembuatan thread bisa lebih alamd ari proses thread tersendiri. namun jika proses dalam thread lebih lama mungkin kebalikannya bisa terjadi.

# Soal 3
## Penjelasan Umum
Pada soal ini terdiri dari 2 program yaitu user.c sebagai identifier serta stream.c sebagai reciever dengan mekanisme :
## user.c 
sebagai sebuah program yang akan mengirim perintah yang bisa dilakukan user seperte decrypt, list, play dan add

## stream.c
sebagai sebuah program yang akan menerima input dari user.c serta menjalankan perintah yang diterima dari user.c

__Penjelasan Code__
## Step 1
```
printf("Enter Command : ");
    char comm[10];
    scanf("%s", comm);
```
Pertama-tama, meminta inputan user untuk perintah yang akan di jalankan dan di masukan ke dalam char comm.

## Step 2
```
if(strcmp(comm,"decrypt")== 0)
    {
        FILE *inputFile = fopen("song-playlist.json", "r");
        if (!inputFile) {
            printf("Failed to open input file\n");
            return 1;
        }

        FILE *outputFile = fopen("sort.txt", "w");
        if (!outputFile) {
            printf("Failed to open output file\n");
            return 1;
        }

        char buffer[100000];
        fread(buffer, 1, 100000, inputFile);
        fclose(inputFile);

        cJSON *json = cJSON_Parse(buffer);
        if (!json) {
            printf("Failed to parse JSON\n");
            return 1;
        }

        int jsonSize = cJSON_GetArraySize(json);
        for (int i = 0; i < jsonSize; i++) {
            cJSON *item = cJSON_GetArrayItem(json, i);
            cJSON *songItem = cJSON_GetObjectItem(item, "song");
            cJSON *methodItem = cJSON_GetObjectItem(item, "method");
    
            char *method = methodItem->valuestring;
            char *song = songItem->valuestring;

            if (strcmp(method, "rot13") == 0) {
                for (int i = 0; i < strlen(song); i++) {
                    if (song[i] >= 'a' && song[i] <= 'z') {
                        song[i] = (song[i] - 'a' + 13) % 26 + 'a';
                    } else if (song[i] >= 'A' && song[i] <= 'Z') {
                        song[i] = (song[i] - 'A' + 13) % 26 + 'A';
                    }
                }
            } else if (strcmp(method, "hex") == 0) {
                int songLength = strlen(song);
                char *decodedSong = malloc(300);
                hex_decode(song, decodedSong);
                strcpy(song, decodedSong);
                free(decodedSong);
            } else if (strcmp(method, "base64") == 0) {
                int songLength = strlen(song);
                int outLength = 0;
                char *decodedSong = malloc(300);
                EVP_DecodeBlock((unsigned char*)decodedSong, (unsigned char*)song, songLength);
                decodedSong[songLength] = '\0';
                strcpy(song, decodedSong);
                free(decodedSong);
            }
            fprintf(outputFile, "%s\n", song);
        }
        fclose(outputFile);
        cJSON_Delete(json);
        system("sort -f sort.txt > playlist.txt");
        system("rm sort.txt");
    }
```
Pertama kita akan mengcompare hasil inputan perintah user dengan "strcmp", jika hasil compare perintah decrypt nya benar maka bisa dijalankan.

```
FILE *inputFile = fopen("song-playlist.json", "r");
        if (!inputFile) {
            printf("Failed to open input file\n");
            return 1;
        }

        FILE *outputFile = fopen("sort.txt", "w");
        if (!outputFile) {
            printf("Failed to open output file\n");
            return 1;
        }
```
Pada line diatas, program akan membuka file json menggunakan "fopen". Lalu di buat pengkondisian jika file tidak ada pada directory maka akan mengeluarkan output "Failed to open input file"

Lalu terdapat line yang akan membuat sort.txt menggunakan fopen dengan "w", sort.txt ini berguna sebagai file temporary dan dibuatkan pengkondisian yang sama seperti diatas.

```
char buffer[100000];
        fread(buffer, 1, 100000, inputFile);
        fclose(inputFile);

        cJSON *json = cJSON_Parse(buffer);
        if (!json) {
            printf("Failed to parse JSON\n");
            return 1;
        }
```

Pada line diatas, buffer data yang telah dibaca dari file input di-parse menggunakan `cJSON_Parse` dari cJSON library. Jika parsing gagal, code akan mencetak pesan error dan mengembalikan nilai 1. Namun jika berhasil, hasil parsing akan disimpan dalam sebuah pointer json yang menunjuk pada struktur data cJSON.

```
int jsonSize = cJSON_GetArraySize(json);
        for (int i = 0; i < jsonSize; i++) {
            cJSON *item = cJSON_GetArrayItem(json, i);
            cJSON *songItem = cJSON_GetObjectItem(item, "song");
            cJSON *methodItem = cJSON_GetObjectItem(item, "method");
    
            char *method = methodItem->valuestring;
            char *song = songItem->valuestring;

            if (strcmp(method, "rot13") == 0) {
                for (int i = 0; i < strlen(song); i++) {
                    if (song[i] >= 'a' && song[i] <= 'z') {
                        song[i] = (song[i] - 'a' + 13) % 26 + 'a';
                    } else if (song[i] >= 'A' && song[i] <= 'Z') {
                        song[i] = (song[i] - 'A' + 13) % 26 + 'A';
                    }
                }
            } else if (strcmp(method, "hex") == 0) {
                int songLength = strlen(song);
                char *decodedSong = malloc(300);
                hex_decode(song, decodedSong);
                strcpy(song, decodedSong);
                free(decodedSong);
            } else if (strcmp(method, "base64") == 0) {
                int songLength = strlen(song);
                int outLength = 0;
                char *decodedSong = malloc(300);
                EVP_DecodeBlock((unsigned char*)decodedSong, (unsigned char*)song, songLength);
                decodedSong[songLength] = '\0';
                strcpy(song, decodedSong);
                free(decodedSong);
            }
            fprintf(outputFile, "%s\n", song);
        }
```
Pertama-tama akan mengambil ukuran array JSON menggunakan cJSON_GetArraySize untuk mendapatkan jumlah item dalam array tersebut. Lalu, untuk setiap item dalam array, code mengambil pointer item menggunakan cJSON_GetArrayItem dengan indeks yang sesuai.

Dalam setiap item, code mengambil nilai song dan method menggunakan cJSON_GetObjectItem dengan masing-masing nama kunci (key) "song" dan "method"

Selanjutnya, akan di lakukan pengcompare an nama method yang ada di dalam file .json dengan 3 method decrypt, yang pertama dengan rot13, hex, dan base64. Program akan menyesuaikan jika isi dari text yang akan di decrypt menggunakna method apa dan akan langsung di decrypt dengan methodnya masing masing.

Pada method ROT13, setiap karakter dalam song akan diambil satu per satu menggunakan looping for dan dicek apakah karakter tersebut merupakan huruf dalam alfabet. Jika iya, karakter tersebut akan digeser sebanyak 13 posisi dan diubah nilai yang lama dengan nilai yang baru. Jika tidak, karakter tersebut akan dilewatkan begitu saja.

Pada method HEX, akan memanggil fungsi hex_decode(). Fungsi hex_decode() menerima sebuah string yang dikodekan dalam format hexadesimal (direpresentasikan sebagai sebuah array karakter) dan mengubahnya menjadi bentuk aslinya, kemudian menuliskan output ke sebuah array karakter yang ditunjuk oleh parameter output.

Pada method BASE64, panjang data lagu dihitung menggunakan fungsi strlen(), dan kemudian digunakan untuk menentukan ukuran buffer hasil dekode yang dibutuhkan. Sebuah buffer sebesar 300 byte dideklarasikan menggunakan fungsi malloc(), dan hasil dekode akan disimpan di dalam buffer tersebut. Selanjutnya, fungsi EVP_DecodeBlock() dipanggil untuk melakukan dekode data lagu dari base64 ke format biner. Parameter pertama adalah pointer ke buffer tempat hasil dekode disimpan, sedangkan parameter kedua adalah pointer ke data lagu yang akan didekode. Parameter ketiga adalah panjang data lagu. Setelah proses dekode selesai, karakter null ('\0') ditambahkan ke akhir buffer hasil dekode, dan hasil dekode disalin ke buffer data lagu menggunakan fungsi strcpy(). Buffer hasil dekode yang telah selesai digunakan kemudian dibebaskan menggunakan fungsi free().

```
fprintf(outputFile, "%s\n", song);
```
Setelah semua lagu di decryot maka hasilnya akan di masukan kedalam outputFile.

```
fclose(outputFile);
        cJSON_Delete(json);
        system("sort -f sort.txt > playlist.txt");
        system("rm sort.txt");
```
Pada code diatas, akan menutup file outputFile. Lalu CJSON_Delete berfungsi menghapus json yang sedang digunakan.

Lalu akan dijalankan fungsi linux untuk mengsortir hasil decrypt. Semua hasil decrypt yang berada pada sort.txt akan di sortir sesuai alpabeth menggunakan `sort -f` dan di pindahkan ke dalam playlist.txt
Setelah dipindahkan, sort.txt akan di hapus menggunakan `rm sort.txt`

```
 else if(strcmp(comm,"list")==0){
        system("cat playlist.txt");
 }
```
Line diatas berfungsi jika user menginputkan perintah list, maka program akan menampilkan hasil dari platlist.txt

```
else if(strcmp(comm,"play")==0){
        char sortir[200];
        scanf(" %[^\n]", sortir);
        char perintah[300];
        snprintf(perintah, 300, "grep -i '%s' playlist.txt > search.txt", sortir);
        system(perintah);

        FILE *count;
            char filename[] = "search.txt";
            char ch;
            int lines = 0;

            count = fopen(filename, "r");

            while ((ch = fgetc(count)) != EOF) {
                if (ch == '\n') {
                    lines++;
                }
            }

            fclose(count);
            if(lines > 1){
                printf("%d song founded within %s name: \n", lines, sortir);
                FILE *fp;
                char filename[] = "search.txt";
                char buffer[1000];
                int num = 0;

                fp = fopen(filename, "r");

                while(fgets(buffer, 1000, fp)) {
                    num++;
                    printf("%d. %s", num, buffer);
                }
                fclose(fp);

                printf("Choose song : ");
                char pilihan[100];
                scanf("%s",pilihan);

                char pilih[300];
                snprintf(pilih, 300, "grep -i '%s' search.txt > pilih.txt", pilihan);
                system(pilih);

                char filename2[] = "pilih.txt";
                
                fp = fopen(filename2, "r");
                
                while(fgets(buffer, 1000, fp)) {
                    printf("Now playing : %s", buffer);
                    break;
                }
                fclose(fp);
                }else{
                char outsearch[100];
                sprintf(outsearch, "cat search.txt | sed 's/^/play /'");
                system(outsearch);
            }
        system("rm search.txt");
        system("rm pilih.txt");
    }
```
Jika user memberikan perintah "play" maka pertama tama program akan meminta inputan untuk nama lagu dengan kondisi spasi antara inputan "play" dengan "namalagu" di pisah arti inputannya menggunakan 
```
scanf(" %[^\n]", sortir);
```
Lalu program akan mencari nama dari lagu yang di input user dengna grap -i di dalam playlist.txt dan akan mensortir nya ke dalam search.txt

```
snprintf(perintah, 300, "grep -i '%s' playlist.txt > search.txt", sortir);
        system(perintah);
```

Lalu akan di hitung berapa banyak hasil sortiran yang ada pada search.txt dengan code berikut.
```
FILE *count;
            char filename[] = "search.txt";
            char ch;
            int lines = 0;

            count = fopen(filename, "r");

            while ((ch = fgetc(count)) != EOF) {
                if (ch == '\n') {
                    lines++;
                }
            }
```
Lalu setelah mendapatkan jumlah dari hasil sortiran, program akan menegluarkan output berapa banyak jumlahnya dan lagunya apa saja.

```
if(lines > 1){
                printf("%d song founded within %s name: \n", lines, sortir);
                FILE *fp;
                char filename[] = "search.txt";
                char buffer[1000];
                int num = 0;

                fp = fopen(filename, "r");

                while(fgets(buffer, 1000, fp)) {
                    num++;
                    printf("%d. %s", num, buffer);
                }
                fclose(fp);
```

(OPSIONAL)

Lalu program akan meminta user untuk memberi inputan detail nama lagu yang diinginkan berdasarkan sortiran yang ada pada search.txt. Lalu program akan mengeluarkan output lagu yang di pilih user.
```
printf("Choose song : ");
                char pilihan[100];
                scanf("%s",pilihan);

                char pilih[300];
                snprintf(pilih, 300, "grep -i '%s' search.txt > pilih.txt", pilihan);
                system(pilih);

                char filename2[] = "pilih.txt";
                
                fp = fopen(filename2, "r");
                
                while(fgets(buffer, 1000, fp)) {
                    printf("Now playing : %s", buffer);
                    break;
                }
                fclose(fp);
                }else{
                char outsearch[100];
                sprintf(outsearch, "cat search.txt | sed 's/^/play /'");
                system(outsearch);
```
Setelah itu program akan menghapus file search.txt dan pilih.txt karena itu merupakan file temporary.
```
system("rm search.txt");
system("rm pilih.txt");
```
Jika user menginput perintah add, maka pertama tama sama seperti perintah play. User akan diminta inputan nama lagu yang akan di tambah.

Lalu akan di hitung juga berapa lagu yang ada dengan nama yang akan di tambah

Jika nama lagu yang akan di tambah berada pada playlist dengan kondisi hasil countnya >=1 , maka akan di keluarkan output "SONG ALREADY ON PLAYLIST"

Jika hasil countnya < 1 maka lagu akan berhasil di tambahkan.

Otomastis lagu akan di tambahkan ke dalam playlist.txt dengan struktur yang tidak beraturan, jadi program akan mengsortir playlist.txt lagi dan dipindahkan ke dalam file temporary newsong.txt.

Lalu hasil sortiran newsong.txt akan di copy kedalam playlist.txt, lalu newsong.txt akan di hapus karena sebuah file temporary.

```
else if(strcmp(comm,"add")==0){
        char newsong[100];
        scanf(" %[^\n]", newsong);
        FILE *addnewsong = fopen("playlist.txt", "a");

        char perintah[150];
        snprintf(perintah, 200, "grep -i '%s' playlist.txt > search.txt", newsong);
        system(perintah);

        FILE *count;
        char filename[] = "search.txt";
        char ch;
        int baris = 0;

        count = fopen(filename, "r");

        while ((ch = fgetc(count)) != EOF) {
            if (ch == '\n') {
                baris++;
            }
        }

        fclose(count);

        if(baris >= 1){
            printf("SONG ALREADY ON PLAYLIST\n");
        }else if(baris < 1){
            fprintf(addnewsong, "%s\n", newsong);
            printf("%s successfully added\n", newsong);
        }
        system("rm search.txt");
        fclose(addnewsong);
        system("sort -f playlist.txt > newsong.txt");
        system("cp newsong.txt playlist.txt");
        system("rm newsong.txt");
    }
```



## Soal 4
### Penjelasan Umum
Pada soal, diminta membuat 3 file program yaitu unzip.c, categorize.c, dan logchecker.c. Ketiga program tersebut memiliki perannya masing-masing. Pada soal diminta untuk mendownload file hehe.zip lalu diekstrak. Setelah itu, diminta untuk memindahkan file sesuai tipe data yang sesuai dengan ketentuan pada file max.txt dan extensions.txt "File yang ada akan dipindahkan ke folder __jpg, txt, js, py, png, emc, xyz__. Masing-masing harus diisi __10__. Jika penuh, maka akan dibuat folder baru dengan tambahan angka di dalam kurung contohnya jpg (2), jpg (3), dan seterusnya. Selain itu akan dipindahkan ke folder other dengan tanpa batasan file". Setelah itu akan dihitung jumlah file sesuai folder yang dibuat (jpg, txt, js, py, png, emc, xyz, dan other). Setiap pengaksesan folder harus menggunakan _multithread_. Pengaksesan tersebut akan dibuat log.txt dengan format:
```
DD-MM-YYYY HH:MM:SS ACCESSED [folder path]
DD-MM-YYYY HH:MM:SS MOVED [extension] file : [source path] > [folder destination]
DD-MM-YYYY HH:MM:SS MADE [folder name]
```
Kemudian log.txt akan dicek dengan mengolah data pada file tersebut menjadi informasi ke terminal. Informasi tersebut adalah banyaknya _accessed_ yang dilakukan, jumlah isi file dalam folder, dan banyaknya total file dalam extension (jpg, txt, js, py, png, emc, xyz, dan other) secara terurut.

#### Penjelasan Program (unzip.c)
Pada program ini harus dapat mengunduh hehe.zip (menggunakan perintah wget) dan mengekstrak file tersebut (menggunakan perintah unzip) dengan tambahan file zip telah dihapus setelah diekstrak (menggunakan perintah rm). Maka program yang dijalankan adalah

```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pwd.h>
#include <wait.h>
#include <sys/types.h>

void remove_file(char *filename) {
  pid_t rmpid;
  rmpid = fork();
  char *argv[] = {"rm", "-q", filename, NULL};
  if(rmpid == 0) {
    execv("/usr/bin/rm", argv);
  }
  while(wait(NULL) != rmpid);
}

void unzip(char *filename) {
  pid_t uzpid;
  uzpid = fork();
  char *argv[] = {"unzip", "-q", filename, NULL};
  if(uzpid == 0) {
    execv("/usr/bin/unzip", argv);
  }
  while(wait(NULL) != uzpid);
  remove(filename);
}

void download() {
  pid_t dlpid;
  dlpid = fork();
  char *argv[] = {"wget", "https://drive.google.com/uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp", "-q", NULL};
  if(dlpid == 0) {
    execv("/usr/bin/wget", argv);
  }
  while(wait(NULL) != dlpid);
}

int main(void) {
  pid_t main_fork;
  main_fork = fork();
  if(main_fork == 0) {
    download();
    exit(0);
  }
  while(wait(NULL) != main_fork);

  main_fork = fork();
  if(main_fork == 0) {
    unzip("uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp");
    exit(0);
  }
  while(wait(NULL) != main_fork);

  return 0;
}

```

#### Penjelasan Program (categorized.c)
Untuk categorize diminta untuk dapat memindahkan file sesuai extension yang tersedia pada extensions.txt dengan batasan sesuai angka pada max.txt. Untuk membaca extensions.txt terdapat _function_ read_extensions() dengan tujuan menjumlah extension yang ada. Kemudian akan dicetak seluruh lokasi file ke all_file.txt. Kemudian program menghitung total file sesuai extension yang tersedia dengan membaca all_file.txt yang telah dibuat barusan. Untuk mencocokan extension dengan tipe data file tersebut terdapat _function_ hasExtension(), jika ketemu maka akan dihitung sesuai extension yang cocok, sebaliknya akan dihitung ke 'other'. Kemudian lakukan pemindahan file dengan multithread. Untuk dapat membawa variabel ke thread, dibuat 'typedef struct' yang berisi lokasi file dan extension, kemudian disalin jika telah menemukan folder yang cocok. Kemudian masuk ke _function_ *move_file(void *arg). Di dalam _function_ tersebut, akan dibuat lokasi tujuan dengan menyalin extension yang telah cocok ke string baru menggunakan 'snprintf' dan memisahkan nama file pada lokasi awal menggunakan 'strrchr'. Setiap kegiatan memindahkan file _function_ multithread tersebut, akan dicetak log pada _function_ log_access(log_type, extension, target, destination) sesuai perintah yang sudah ada di dalam log_type. Kemudian dicetak ke log.txt dengan format yang telah disebutkan pada penjelasan umum

##### Belum dikerjakan
- Menaruh jumlah extension sebagai variabel baru untuk membatasi looping di _function_ countExtensions()
- Jika file telah mencapai batas yang ditentukan, maka akan ditambah folder baru dengan angka seperti contoh 'jpg (2)'. Kemudian folder tersebut dijadikan tujuan baru untuk file yang akan dipindah selanjutnya.
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <pwd.h>
#include <sys/stat.h>
#include <unistd.h>
#include <wait.h>
#include <pthread.h>
#include <stdbool.h>
#include <ctype.h>

typedef struct {
  char filename[512];
  char data_type[20];
} moving;

int read_extensions() {
  FILE *dt = fopen("extensions.txt", "r");
  char data_type[20][20];
  int type_count = 0;
  while (fgets(data_type[type_count], 20, dt)) {
    type_count++;
  }
  fclose(dt);
  return type_count;
}

bool hasExtension(char filename[], char data_type[]) {
  char *dot = strrchr(filename, '.');
  if (dot && !strcmp(dot + 1, data_type)) {
    return 1;
  }
  return 0;
}

void log_access(char *log_type, char *extension, char *target, char *destination) {
  FILE *dt7 = fopen("log.txt", "w+");
  time_t time_;
  struct tm *time_now;
  char timestamp[24];

  time_ = time(NULL);
  time_now = localtime(&time_);
  strftime(timestamp, sizeof(timestamp), "%d-%m-%Y %H:%M:%S", time_now);

  if(!strcmp(log_type, "ACCESSED")) {
    fprintf(dt7, "%s %s %s\n", timestamp, "ACCESSED", target);
  } else if(!strcmp(log_type, "MOVED")) {
    fprintf(dt7, "%s %s %s file : %s > %s\n", timestamp, "MOVED", extension, target, destination);
  } else if(!strcmp(log_type, "MADE")) {
    fprintf(dt7, "%s %s %s\n", timestamp, "MADE", destination);
  }
  fclose(dt7);
}

void *move_file(void *arg) {
    moving *file = (moving *) arg;

    char dir_name[255];
    snprintf(dir_name, sizeof(dir_name), "./categorized/%s", file->data_type);

    char *filename_s = strrchr(file->filename, '/');
    if (access(dir_name, F_OK) == -1) {
        mkdir(dir_name, 0777);
        log_access("MADE", NULL, NULL, dir_name);
    }

    char command[1400];
    snprintf(command, sizeof(command), "mv %s %s%s", file->filename, dir_name, filename_s);
    system(command);

    log_access("ACCESSED", NULL, file->filename, NULL);
    log_access("MOVED", file->data_type, file->filename, dir_name);

    free(file);
    pthread_exit(NULL);
}

void countExtensions() {
  char data_type[20][20];
  FILE *dt2 = fopen("extensions.txt", "r");

  int type_count = 0;
  while (type_count != 8) {
    fscanf(dt2, "%s", data_type[type_count]);
    //printf("%s\n", data_type[type_count]);
    type_count++;
  }
  fclose(dt2);

  int extension_total = type_count;
  int counter[extension_total + 1];
  for (int i = 0; i <= extension_total; i++) {
    counter[i] = 0;
  }
  FILE *dt4 = fopen("all_file.txt", "r");
  int total_file = 0;
  while(total_file != 200) {
    char data_type2[10], file_location2[255];
    fscanf(dt4, "%s", file_location2);
    FILE *dt3 = fopen("extensions.txt", "r");
    int foundExtension = 0;
    for (int i = 0; i < extension_total; i++) {
      fscanf(dt3, "%s", data_type2);
      //printf("%s %s\n", file_location2, data_type2);
      if(hasExtension(file_location2, data_type2)) {
        foundExtension = 1;
        counter[i]++;
        break;
      }
    }
    if (!foundExtension) {
      counter[extension_total]++;
    }
    fclose(dt3);
    total_file++;
  }
  fclose(dt4);

  printf("File Extension\n");
  for (int i = 0; i < extension_total; i++) {
    printf("%s    : %d\n", data_type[i], counter[i]);
  }
  printf("Other : %d\n", counter[extension_total]);
}


int main() {
  system("mkdir categorized; date +%d-%m-%Y %H:%M:%S MADE categorized\n > log.txt; find ./files -type f > all_file.txt");
  countExtensions();

  pthread_t thread_id[1024];
  int thread_count = 0, total_file2 = 0;
  int type_total2 = read_extensions();
  FILE *dt5 = fopen("all_file.txt", "r");
  while(total_file2 != 200) {
    char file_location3[255], data_type3[255];
    fscanf(dt5, "%s", file_location3);
    FILE *dt6 = fopen("extensions.txt", "r");
    for(int i = 0; i < type_total2; i++) {
      fscanf(dt6, "%s", data_type3);
      if(hasExtension(file_location3, data_type3)) {
        moving *file = malloc(sizeof(moving));
        strcpy(file->filename, file_location3);
        strcpy(file->data_type, data_type3);

        pthread_create(&thread_id[thread_count], NULL, move_file, file);
        thread_count++;
        break;
      }
    }
  }
  for(int i = 0; i < thread_count; i++) {
    pthread_join(thread_id[i], NULL);
  }
  fclose(dt5);

  return 0;
}


```

#### Penjelasan Program (logchecker.c)
Pada logchecker.c diminta untuk membaca log.txt. Pada log.txt telah terdapat tulisan pasti yaitu 'ACCESSED', 'MADE', 'MOVED'. Jika pada fscanf 'word' mendeteksi ketiga tulisan tersebut, maka akan masuk ke if-else sesuai namanya. ACCESSED hanya memiliki folder_path, sehingga jumlah accessed bertambah. Kemudian MADE akan mendeteksi bahwa file ini telah dibuat, maka nama folder ditulis dengan posisi accessed folder tersebut masih 0. Kemudian MOVED terdapat tiga variabel, yaitu 'extension', 'source path', dan 'folder destination'.

##### Belum dikerjakan
- Extension digunakan untuk menghitung jumlah file sesuai extension, source path dan folder destination digunakan untuk menambahkan jumlah accessed
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

int main() {
  FILE *log_reader = fopen("log.txt", "r");
  int access = 0, folder_max = 0, limit = 0, folder_count[100];
  char folder_list[100][20];
  while(limit != 100) {
    char word[255];
    fscanf(log_reader, "%s", word);
    //printf("%s\n", word);
    if(!strcmp(word, "ACCESSED")) {
      access++;
    }
    else if(!strcmp(word, "MADE")) {
      fscanf(log_reader, "%s", folder_list[folder_max]);
      folder_count[folder_max] = 0;
      folder_max++;
    }
    else if(!strcmp(word, "MOVED")) {
      char char_moved[5];
      fscanf(log_reader, "%s", word);
      fscanf(log_reader, "%s", char_moved);
      if(!strcmp(char_moved, ">")) {
        char location[255];
        fscanf(log_reader, "%s", location);
        for(int i = 0; i < folder_max; i++) {
          if(!strcmp(folder_list[i], location)) {
            folder_count[i]++;
            break;
          }
        }
      }
    }
    limit++;
  }
  printf("total accessed: %d\n", access);
  for(int i = 0; i < folder_max; i++) {
    printf("%s     : %d\n", folder_list[i], folder_count[i]);
  }
  fclose(log_reader);
  return 0;
}

```

#### Kendala
Kendala pada saat mengoding adalah pada pthread, di mana terjadi crash saat memindahkan file ke folder tujuan. Akibatnya hanya sebagian file saja yang dapat dipindah dengan hanya 1 log yang dapat dicetak. Karena itu, terdapat beberapa dugaan. Pertama, terdapat kesalahan pada program untuk memasukan alamat (sumber/tujuan) file sehingga terdapat pesan "such no file or directory". Kedua, terjadi _race condition_ pada pencatatan log, sehingga log yang ditulis hanya 1. Untuk dugaan lainnya, masih belum diketahui yang dapat menyebabkan muncul "segmentation fault" 20-30 detik setelah tidak merespon. Akibat hal tersebut, membuat waktu tidak cukup dalam menyelesaikan program lochecker.c (menghitung jumlah file sesuai _extension_, jumlah file diakses tidak terurut _acsending_)
